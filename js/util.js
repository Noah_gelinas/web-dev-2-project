"use strict";

/**
 * Returns array which has 3 number values which represent R G and B
 * @param {string} color - Gets an array of RGB numbers from a color string in CSS rgb format
 * @returns {[number]} Color channels
 */
function getColorChannels(color) {
    return Array.from(color.match(/rgb\((\d+), (\d+), (\d+)\)/))
        .slice(1, 4)
        .map((a) => parseInt(a));
}

/**
 * Finds most dominant color and returns the string color
 * @param {string} color - Color string in CSS rgb format
 * @returns {["red" | "green" | "blue"]} Array of color names that are most predominant
 */
function getPredominantColors(color) {
    let channels = getColorChannels(color);
    let highestChannels = [];
    const channelNames = ["red", "green", "blue"]

    for (let i = 0; i < channels.length; i++) {
        if (Math.max(...channels) === channels[i]) {
            highestChannels.push(channelNames[i]);
        }
    }
    return highestChannels;
}

/**
 * Determines if a color is dark or light
 * @param {string} color - Color string in CSS rgb syntax
 * @returns {boolean} Returns whether the color is dark or not
 * @see {@link https://www.w3.org/TR/AERT/#color-contrast:~:text=Color%20brightness%20is%20determined%20by%20the%20following%20formula%3A}
 */
function isDarkColor(color) {
    const [r, g, b] = getColorChannels(color);
    const brightness = (r * 299 + g * 587 + b * 114) / 1000;
    return brightness < 128;
}

/**
 * Counts amount of tiles matching the player's chosen color
 * @returns {number} Amount of tiles whose highest color channel is the player's selected color
 */
function getPredominantTileCount() {
    const preColor = document.getElementById("color").value;
    let boardSection = document.getElementById("gameBoard");
    const tileCount = Array.from(boardSection.getElementsByTagName("td"))
        .map((td) => td.style.backgroundColor)
        .map((clr) => getPredominantColors(clr))
        .filter((pc) => pc.includes(preColor)).length;
    return tileCount;
}
