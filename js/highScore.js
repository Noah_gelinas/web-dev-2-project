"use strict";

/**
 * Generates a score based on the player's most recently played game
 * @param {number} numCorrect - Number of tiles player chose correctly
 * @param {number} numSelected - Number of tiles player selected total
 * @param {number} boardSize - Size of board
 * @param {number} difficulty - How difficult the game was
 * @returns {number} Score
 */
function getScore(numCorrect, numSelected, boardSize, difficulty) {
	const percent = (2 * numCorrect - numSelected) / (boardSize * boardSize);
	return Math.floor(percent * 100 * boardSize * (difficulty + 1));
}

/**
 * Gets a list of high scores
 * @returns { {[playerName: string]: number} } Object of names associated to scores
 */
function getHighScores() {
	return JSON.parse(localStorage.getItem("highScores")) || {};
}

/**
 * Adds high score to highscore table
 * @param {string} pName - Player's selected name
 * @param {number} score - Player's score
 */
function addHighScore(pName, score) {
	const highScores = getHighScores();

	const MAX_HIGH_SCORES_ALLOWED = 10;
	const totalAmtOfHighScores = Object.keys(highScores).length;
	const amtOfHigherScores = Object.values(highScores).filter((hs) => hs > score).length;
	const isAlreadyInList = Object.keys(highScores).includes(pName);

    // Exit if the player is already in the leaderboard but did not beat their previous high score
	if (isAlreadyInList && highScores[pName] > score) return;

    // Exit if the player's score isn't in the top 10 scores
	if (amtOfHigherScores === MAX_HIGH_SCORES_ALLOWED) return;

    // Knock off the lowest scoring player to make space for the new high score if the list is already full
	if (totalAmtOfHighScores === MAX_HIGH_SCORES_ALLOWED && !isAlreadyInList) {
		let lowestScoringPlayer;
		for (let player in highScores) {
			if (!lowestScoringPlayer) {
				lowestScoringPlayer = player;
			} else if (highScores[lowestScoringPlayer] > highScores[player]) {
				lowestScoringPlayer = player;
			}
		}
		if (lowestScoringPlayer) delete highScores[lowestScoringPlayer];
	}
	highScores[pName] = score;
	localStorage.setItem("highScores", JSON.stringify(highScores));
}

/**
 * Generates the highscore table in the DOM
 * @param {"ascending" | "descending"} order - Order of the table
 */
function generateHighScoreList(order = "descending") {
	const hsTableBody = document.querySelector("#highScores tbody");
	emptyHighScoreTable();
	/**
	 * Generates an entry for the high score table in the DOM.
	 * @param {string} pName - Player's selected name
	 * @param {number} score - Player's score
	 */
	function generateHighScoreObj(pName, score) {
		const hsEntry = document.getElementById("highScoreTemplate").content.cloneNode(true);
		const [nameTd, scoreTd] = hsEntry.querySelectorAll("td");
		nameTd.textContent = pName;
		scoreTd.textContent = score;
		hsTableBody.append(hsEntry);
	}

	const ascendingSort = (k1, k2) => highScores[k2] - highScores[k1];
	const descendingSort = (k1, k2) => highScores[k1] - highScores[k2];

	const highScores = getHighScores();
	Object.keys(highScores)
		.sort(order === "descending" ? ascendingSort : descendingSort)
		.forEach((key) => generateHighScoreObj(key, highScores[key]));
}

/**
 * Empty the highSscore table.
 */
function emptyHighScoreTable() {
	const hsTable = document.getElementById("highScores");
	hsTable.querySelectorAll(".highScoreTableEntry").forEach((e) => e.remove());
}

/**
 * DOMContentLoaded event listener on the document
 */
document.addEventListener("DOMContentLoaded", function () {
	generateHighScoreList();

	let deleteHsButton = document.getElementById("deleteHsTable");

	/**
	 * Event listener on the delete button which deletes the high score table and removes the obj from local storage
	 */
	deleteHsButton.addEventListener("click", function (e) {
		localStorage.removeItem("highScores");
		emptyHighScoreTable();
	});

	let changeDescToAsc = document.getElementById("switchOrder");

	/**
	 * Event listener on h1 which switches order of highscore table.
	 */
	changeDescToAsc.addEventListener("click", function (e) {
		let HSTable = document.getElementById("highScores");

		if (HSTable.classList.contains("desc")) {
			HSTable.classList.replace("desc", "asc");
			generateHighScoreList("ascending");
		} else if (HSTable.classList.contains("asc")) {
			HSTable.classList.replace("asc", "desc");
			generateHighScoreList("descending");
		}
	});
});
