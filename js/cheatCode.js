"use strict";

/**
 * Toggles the cheat mode visibility
 * @param {boolean} on Whether to turn cheat code on or not
 */
function toggleCheatCode(on) {
    const boardSection = document.getElementById("gameBoard");
    const existingGameBoard = boardSection.querySelector("table");
    if (!existingGameBoard) return;

    const allTd = boardSection.querySelectorAll("td");

    if (on) {
        allTd.forEach((td) => {
            let highestColors = getPredominantColors(td.style.backgroundColor);
            td.textContent = `${td.style.backgroundColor}\n${highestColors.join(", ")}`;
            if (isDarkColor(td.style.backgroundColor)) td.style.color = "white";
        })
    }
    else {
        allTd.forEach((td) => (td.textContent = ""));
    }
}

/**
 * DOMContentLoaded eventlistener
 */
document.addEventListener("DOMContentLoaded", function (e) {
    /**
     * KeyDown event listener on the body to toggle cheatCode.
     */
    document.body.addEventListener("keydown", function (e) {
        const boardSection = document.getElementById("gameBoard");
        const existingGameBoard = boardSection.querySelector("table");
        if (!existingGameBoard) return;
        if (e.target.tagName === "INPUT") return;
        
        let existingCheatCode = boardSection.querySelector("td");
        if (!e.ctrlKey && !e.altKey && e.shiftKey && e.code === "KeyC") {
            if (existingCheatCode && existingCheatCode.textContent === "") {
                toggleCheatCode(true);
            }
            else {
                toggleCheatCode(false);
            }
        }
    });
});
