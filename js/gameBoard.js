"use strict";
/**
 *
 * @param {number} rowAndCol - Number of rows and columns
 * @param {number} difficultyRange - Difficulty chosen by the player
 * @param {string} color - Color the player chose to search for
 */
function createTable(rowAndCol, difficultyRange, color) {
    /**
     * Generates the random color for a tile
     * @returns {string} String of RGB colors in CSS rgb syntax
     */
    function generateRandomColor() {
        /**
         * Returns a random integer between a and b
         * @param {number} a - Minimum bound
         * @param {number} b - Maximum bound
         * @returns {[string]} Random integer between a and b
         */
        function randomFromRange(a, b) {
            return Math.floor(Math.random() * (b - a)) + a;
        }

        const pivot = randomFromRange(0, 255 - difficultyRange);
        const rgb = [];
        for (let i = 0; i < 3; i++) {
            rgb.push(randomFromRange(pivot, pivot + difficultyRange));
        }
        return `rgb(${rgb.join(", ")})`;
    }

    const existingBoard = document.getElementById("gameBoard").querySelector("table");
    if (existingBoard) existingBoard.remove();

    const newBoard = document.createElement("table");
    const gameBoardSection = document.getElementById("gameBoard");
    const pElement = gameBoardSection.getElementsByTagName("p")[0];
    gameBoardSection.insertBefore(newBoard, pElement);

    for (let i = 0; i < rowAndCol; i++) {
        const newRow = document.createElement("tr");
        newBoard.appendChild(newRow);
        for (let x = 0; x < rowAndCol; x++) {
            const newCol = document.createElement("td");
            newCol.style.backgroundColor = generateRandomColor();
            newRow.appendChild(newCol);
        }
    }

    while(getPredominantTileCount() === 0) {
        newBoard.querySelectorAll("td").forEach(tile => {
            tile.style.backgroundColor = generateRandomColor();
        })
    }
    pElement.textContent = `Search for ${color} tiles! Your target is ${getPredominantTileCount()}`;

    /**
     * Event listener which listens for a click on a tile then gives it a class or removes a class.
     */
    newBoard.addEventListener("click", function (e) {
        if (e.target.tagName !== "TD") return;

        if (e.target.classList.contains("selected")) e.target.classList.remove("selected");
        else e.target.classList.add("selected");
    });
}

/**
 * DOMContentLoaded event listener on the document
 */
document.addEventListener("DOMContentLoaded", function (e) {
    const submitGuessButton = document.getElementById("submitGuess");
    submitGuessButton.addEventListener("click", function (e) {
        const preColor = document.getElementById("color").value;
        let selectedTiles = document.querySelectorAll(".selected");
        let guessedCorrectly = 0;
        let numSelected = 0;
        let tileCount = getPredominantTileCount();
        const pElement = document.getElementsByTagName("p")[0];

        for (let selectedTile of selectedTiles) {
            let matchTiles = getPredominantColors(selectedTile.style.backgroundColor);
            if (matchTiles.includes(preColor)) {
                guessedCorrectly++;
            }
            numSelected++;
        }
		const boardSize = document.getElementById("boardSize").value;
        const pName = document.getElementById("playerName").value;
        const difficulty = document.getElementById("difficulty").value;

        const score = getScore(guessedCorrectly, numSelected, boardSize, difficulty);
        const percent = Math.floor((guessedCorrectly / Math.max(numSelected, tileCount)) * 100);

        if (guessedCorrectly === tileCount) {
            if (guessedCorrectly < numSelected) {
                pElement.textContent = `You successfully guessed ${guessedCorrectly}/${tileCount} tiles, but you selected an extra ${numSelected - guessedCorrectly} tile(s).`;
            }
            else {
                pElement.textContent = `You succesfully guessed all the correct tiles!`
            }
        }
        else if (guessedCorrectly < tileCount) {
            if (guessedCorrectly < numSelected) {
                pElement.textContent = `You only guessed ${guessedCorrectly}/${tileCount} tiles, and you selected an extra ${numSelected - guessedCorrectly} tile(s).`;
            }
            else {
                pElement.textContent = `You did not guess all of the correct tiles, but you got ${guessedCorrectly}/${tileCount}.  Good try.`
            }
        }
		pElement.textContent += ` Your selection was ${percent}% correct and your score is ${score}.`

        addHighScore(pName, score);
        generateHighScoreList();

        let fieldsets = document.querySelectorAll("fieldset");
        fieldsets.forEach((fs) => (fs.disabled = false));
        startGameButton.disabled = false;
        submitGuessButton.disabled = true;

        toggleCheatCode(true);
    });
});
