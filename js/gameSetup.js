"use strict";

/**
 * DOMContentLoaded evevnt listener on the document
 */
document.addEventListener("DOMContentLoaded", function (e) {
    const setupForm = document.getElementById("setup-form");
    const startGameButton = document.getElementById("startGameButton");
    const submitGuessButton = document.getElementById("submitGuess");
    submitGuessButton.disabled = true;

    const DIFFICULTY_COLOR_RANGES = [255, 80, 40, 10];

    /**
     * Changes theme color of header, footer and start game button
     * @param {"red" | "green" | "blue"} color - Color to set as the theme color
     */
    function changeThemeColor(color) {
        const HUES = {
            red: 0,
            green: 120,
            blue: 200,
        };
        const makeHslColor = (hue, lightness) => `hsl(${hue}deg, 100%, ${lightness}%)`;

        startGameButton.style.backgroundColor = makeHslColor(HUES[color], 75);
        document.querySelectorAll("header, footer").forEach((e) => (e.style.backgroundColor = makeHslColor(HUES[color], 92)));
    }

    /**
     * event listener on the submit button of the form which calls the create table function
     */
    setupForm.addEventListener("submit", (e) => {
        e.preventDefault();
        const boardSize = e.target.boardSize.valueAsNumber;
        const predomColor = e.target.color.value;
        const difficulty = e.target.difficulty.value;

        createTable(boardSize, DIFFICULTY_COLOR_RANGES[difficulty], predomColor);
        let fieldsets = document.querySelectorAll("fieldset");
        fieldsets.forEach((fs) => (fs.disabled = true));
        startGameButton.disabled = true;
        submitGuessButton.disabled = false;
    });

    /**
     * event listener to change the page theme color when the user changes predominant color
     */
    setupForm.color.addEventListener("change", (e) => {
        changeThemeColor(e.target.value.toLowerCase());
    });

    // theme color setup
    changeThemeColor(setupForm.color.value.toLowerCase());
});
