# Web Dev 2 Project

Created by **Noah Gelinas** and **Andrew Rokakis**

## Concept

This project is a color guessing game where you enter a username, board size and the color you wish to search for. Then, you are given a board with randomly generated colors and you are given a message representing how many tiles you have to try and select, it is your job to try and determine which tiles RGB values has the highest of your chosen color. Depending on how many you get right and/or wrong you will be given a score along with a message telling you the percentage you got right. Your highscore is set in a table and will stay there even after you refresh until you press the delete scores table. It is sorted in descending order but can be switched to ascending with a click of the "High Scores" title.

## Page layout

The webpage is separated into 3 sections. The first section is the game setup options. The second section is the game board, it takes up the most space on the page. Next to the game board is the leaderboard, showing the top highest scores.

When playing on a mobile phone, the page layout will change, displaying each section vertically.

## Game setup options

These are the options that are available for the game setup. When an input field is invalid, it will be highlighted in red.

When you select a different `Color` option, it will change the webpage's theme to that color.
| Option | Description | Constraints |
|-|-|-|
| Player Name | Your name. It will show up in the leaderboard if you get a high score. | Alphabetical only, minimum 5 characters |
| Game Board Size | The size of the game board. | 3 to 7 |
| Color | The color that you will attempt to identify | Red, Green, Blue |
| Difficulty | How difficult the game is. The higher the difficulty, the closer together the colors will be. | 0 (Easy) to 3 (Really Hard) |

## Game start

When you press *Start Game!*, the game setup options will be disabled until you submit your guess.

You will be presented with an `n` by `n` table, with `n` being the value you put in the `Game Board Size` field. You will be prompted to select a number of tiles (from 1 to the size of your board) that you think contain the highest amount of the color you selected in the game setup form.

## Tile selection

When you click on a tile, it will select it as part of your guess. If you change your mind, you can click on the tile again to de-select it.

Selected tiles will be surrounded by a orange border and a box shadow.

## Submit guess

When you submit your guess, you will be told how many tiles you selected correctly and how many extra tiles you selected (if applicable). You will be shown your percentage guessed correctly and your score.

Along with this information, the game board will display the answers.

Upon submitting your guess, the game setup options will be re-enabled, allowing for another round to be played.

## Leaderboard

The 10 highest scores will be displayed in the leaderboard along with the player's name. These scores are stored in your web browser and will persist unless you press the `Delete Scores` button.

## Cheat mode

When enabled, cheat mode will show the RGB values of each tile along with which color(s) are the highest.

When you press <kbd>Shift</kbd> + <kbd>C</kbd>, it will toggle cheat mode. Cheat mode cannot be toggled if you are currently in an active input field.